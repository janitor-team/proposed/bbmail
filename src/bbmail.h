// bbmail.hh fr bbmail - an tool to display new mail in X11.
//
//  Copyright (c) 1998-2005 John Kennis, jkennis@chello.nl
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// (See the included file COPYING / GPL-2.0)
//


#ifndef __BBMAIL_H
#define __BBMAIL_H

#include <X11/Xutil.h>
#include <X11/Xatom.h>

// Blackbox library includes
#include "Timer.hh"
#include "Display.hh"
#include "Application.hh"
#include "Texture.hh"
#include "PixmapCache.hh"
#include "EventHandler.hh"
#include "Pen.hh"

#include "resource.h"
#include "mailboxmenu.h"
#include "main.h"

#define LEFT_BUTTON 1
#define MIDDLE_BUTTON 2
#define RIGHT_BUTTON 3

class Resource;
class MailboxMenu;
class ToolWindow;

class LocalSpoolfile
{
  	public:
		LocalSpoolfile(void) { last_size = 0; last_mtime = 0; newmail = 0; totalmail = 0; }
		~LocalSpoolfile(void) { }
		time_t last_mtime;
		off_t last_size;
		int newmail;
		int totalmail;
};

class Checkmail : public bt::TimeoutHandler
{
public:
	Checkmail(ToolWindow *);
	virtual ~Checkmail(void);
	void reconfigure(void);
	int getNewmail(void)
	{
		return(newmail);
	}
	int getLastTotalmail(void)
	{
		return(last_totalmail);
	}
	bool getBroken(void)
	{
		return(broken);
	}
	int getNewMailbox(void)
	{
		return(new_mailbox);
	}
	void forceCheck(void);


protected:
	virtual void timeout(bt::Timer *timer);
	
private:
	bool CheckOther(int,bool);
	bool CheckMbox(int);
	bool CheckMaildir(int);
	int ReadPipe(FILE *,int *,int);
	bool checkStatusFlag(char *,int);
	bool getChar(const char *,int *);

	struct LocalSpoolfile *spoolfile;
	ToolWindow *m_pBbtool;
	bt::Timer *timer;
	int new_mailbox;
	int last_totalmail;
	int last_newmail;
	int newmail;
	bool broken;
	unsigned int number_of_checks;

	Resource *resource;

};

class FrameWindow : public bt::EventHandler
{
	public:
		FrameWindow(ToolWindow *toolwindow);
		~FrameWindow(void);
		Window window(void) { return fwindow; }
		void reconfigure(void);
		void createWindow(void);

        void redraw(void);

		void setX(int _x) { fx = _x; }
		void setY(int _y) { fy = _y; }
		void setWidth(int _w) { fwidth = _w; }
		void setHeight(int _h) { fheight = _h; }

		int x(void) { return fx; }
		int y(void) { return fy; }
		unsigned int width(void) { return fwidth; }
		unsigned int height(void) { return fheight; }
        Pixmap pixmap(void) { return m_pixmap; }

	protected:
		virtual void clientMessageEvent(const XClientMessageEvent *xclient_event);
		virtual void exposeEvent(const XExposeEvent *);
		virtual void buttonPressEvent(const XButtonEvent *xbutton_event);
		virtual void configureNotifyEvent(const XConfigureEvent *configure_event);

	private:
		ToolWindow *m_pBbtool;
		Window fwindow;
		int fx;
		int fy;
		unsigned int fwidth;
		unsigned int fheight;
		Pixmap m_pixmap;
		char *fposition;
		int m_screen;
		::Display *display;
		Resource *resource;

};

class LabelWindow : public bt::EventHandler
{
	public:
		LabelWindow(ToolWindow *toolwindow);
		~LabelWindow(void);
		Window window(void) { return m_window; }
		void reconfigure(void);
		void createWindow(void);
		void redraw(void);
		
		void setX(int x_coord) { m_x = x_coord; }
		void setY(int y_coord) { m_y = y_coord; }
		void setHeight(int h) { m_height = h; }

        void calcSize();

		int x(void) { return m_x; }
		int y(void) { return m_y; }
		unsigned int width(void) { return m_width; }

		unsigned int height(void) { return m_height; }
	protected:
		virtual void buttonPressEvent(const XButtonEvent *xbutton_event);
    	virtual void exposeEvent(const XExposeEvent *);

	private:
		ToolWindow *m_pBbtool;

		::Display *m_pDisplay;

		Window m_window;
		Pixmap m_pixmap;
		int m_x;
		int m_y;
		unsigned int m_width;
        unsigned int m_widthNewMail;
        unsigned int m_widthTotalMail;
        unsigned int m_widthSeperator;
		unsigned int m_height;
		bt::Pen *m_pPen;
    	int m_screen;
       	int m_maxValue;
};

class EnvelopeWindow : public bt::EventHandler
{
	public:
		EnvelopeWindow(ToolWindow *toolwindow);
		~EnvelopeWindow(void);
		Window window(void) { return m_window; }
		void reconfigure(void);
		void createWindow(void);
		void redraw(void);
		
		void setX(int _x) { ex = _x; }
		void setY(int _y) { ey = _y; }
		void setWidth(int _w) { ewidth = _w; }
		void setHeight(int _h) { eheight = _h; }

		int x(void) { return ex; }
		int y(void) { return ey; }
		unsigned int width(void) { return ewidth; }
		unsigned int height(void) { return eheight; }

		bool isEnvelopePushed(void) { return envelope_pushed; }
		void envelopePushed(bool _envelope_pushed) { envelope_pushed = _envelope_pushed; }

		void release(void);
		
	protected:
		virtual void buttonPressEvent(const XButtonEvent *xbutton_event);
		virtual void buttonReleaseEvent(const XButtonEvent *xbutton_event);
        virtual void exposeEvent(const XExposeEvent *);
        virtual void calcPoints();

	private:
        ::Display *display;
		ToolWindow *m_pBbtool;
		int m_screen;
		Window m_window;
		int ex;
		int ey;
		unsigned int ewidth;
		unsigned int eheight;
		Pixmap pixmap;
		Pixmap pixmap_pressed;
		Pixmap *pixmap_new;
		Pixmap *pixmap_new_pressed;
		bt::Pen *epen;
		XPoint points[11];
		bool envelope_pushed;
		int num_of_pixmaps;
};

class ToolWindow : public bt::Application
{
public:
	ToolWindow(Configuration cml_options);
	~ToolWindow(void);

	void reconfigure(void);
	void redraw(bool);


	Resource *getResource(void) { return resource; }

	MailboxMenu *mbMenu(void) { return mbmenu; }
	Checkmail *checkMail(void) { return checkmail; }

	bool isRaised(void) { return raised; }
	void setRaised(bool _raised) { raised = _raised; }

	void makeWindow(bool reconfigure);
	void setHeight(int _height) { height = _height; }

	FrameWindow *frameWindow(void) { return frame_window; }
	LabelWindow *labelWindow(void) { return label_window; }
	EnvelopeWindow *envelopeWindow(void) { return envelope_window; }

	//char *spoolDir(void) { return spooldir; }

	void shutdown(void) { setRunState(SHUTDOWN); }
	
	Configuration &config(void) { return _config; }
	int getCurrentScreen(void) { return current_screen; }
	const bt::ScreenInfo *getCurrentScreenInfo(void) { return &current_screen_info; }

	Atom wmDeleteWindowAtom(void) { return wm_delete_window; }

protected:
	bool raised;

private:
//	char *spooldir;
	Checkmail *checkmail;
	FrameWindow *frame_window;
	LabelWindow *label_window;
	EnvelopeWindow *envelope_window;
	Resource *resource;
	MailboxMenu *mbmenu;
	bool mapped;
	unsigned int height;

	Atom wm_delete_window;
	const bt::ScreenInfo &current_screen_info;
	int current_screen;

	Configuration _config;
};

#endif /* __BBMAIL_H */

