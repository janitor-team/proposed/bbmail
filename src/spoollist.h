//  spoollist.h for bbmail - an tool to display new mail in X11.
//
//  Copyright (c) 1998-2005 John Kennis, jkennis@chello.nl
//
//  this program is free software; you can redistribute it and/or modify
//  it under the terms of the gnu general public license as published by
//  the free software foundation; either version 2 of the license, or
//  (at your option) any later version.
//
//  this program is distributed in the hope that it will be useful,
//  but without any warranty; without even the implied warranty of
//  merchantability or fitness for a particular purpose.  see the
//  gnu general public license for more details.
//
//  you should have received a copy of the gnu general public license
//  along with this program; if not, write to the free software
//  foundation, inc., 675 mass ave, cambridge, ma 02139, usa.
//
// (see the included file copying / gpl-2.0)
//


#ifndef __SPOOLLIST_H
#define __SPOOLLIST_H

#include "Texture.hh"
#include "Font.hh"

class SpoolList
{

public:
	SpoolList(void);
	~SpoolList(void);

	enum TYPE {mbox,maildir,other} type;
	std::string filename;
	std::string new_maildir;
	std::string cur_maildir;
    bt::ustring name;
	bt::Color newmail_textColor;
	bt::Color newmail_error_textColor;
	bt::Texture newmail_texture;
	bt::Texture newmail_pressed_texture;
	std::string runCommand;
	std::string statusflag;
	bool procDefined;
	std::string proc;
	std::string reversedProc;
	std::string newmailProc;
	bool force_check;
	bool bell;
	bool first_is_new;
	bool second_is_total;
	bool second_ignore;
};

#endif /* __SPOLLIST_H */

