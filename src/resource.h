// resource.h for bbmail - an tool to display new mail in X11.
//
//  Copyright (c) 1998-2005 John Kennis, jkennis@chello.nl
//
//  this program is free software; you can redistribute it and/or modify
//  it under the terms of the gnu general public license as published by
//  the free software foundation; either version 2 of the license, or
//  (at your option) any later version.
//
//  this program is distributed in the hope that it will be useful,
//  but without any warranty; without even the implied warranty of
//  merchantability or fitness for a particular purpose.  see the
//  gnu general public license for more details.
//
//  you should have received a copy of the gnu general public license
//  along with this program; if not, write to the free software
//  foundation, inc., 675 mass ave, cambridge, ma 02139, usa.
//
// (see the included file copying / gpl-2.0)
//


#ifndef __RESOURCE_H
#define __RESOURCE_H

#include "spoollist.h"
#include "baseresource.h"
#include "Texture.hh"

struct FRAME
{
	int width;
	int height;
	bt::Texture texture;
	int bevelWidth;
	bt::Font font;
};

struct POSITION
{
	int x;
	int y;
	int mask;
};

struct LABEL
{
	int width;
	int width0, width1, width2, width3;
	int height;
	bool transparent;
	bt::Texture texture;
	bt::Font font;
	bt::Color newmail_textColor;
	bt::Color totalmail_textColor;
	bt::Color seperator_textColor;
	bt::Color newmail_boring_textColor;
	bt::Color totalmail_boring_textColor;
	bt::Color seperator_boring_textColor;
};


struct ENVELOPE
{
	int width;
	int height;
	bt::Texture texture;
	bt::Texture texture_pressed;
	bt::Color textColor;
	bt::Color error_textColor;
	int bevelWidth;
	int bevelHeight;
};

struct SHOW
{
	bool newmail_counter;
	bool totalmail_counter;
	bool label;
	bool envelope;
	bool onlyAtNewMail;
};

struct REPORT
{
	bool auto_raise;
	unsigned int check_delay;
	unsigned int force_check_delay;
	bool force_check;
	unsigned int number_of_spoolfiles;
	int number_of_digits;
	std::string runCommand;
};

class ToolWindow;

class Resource : public BaseResource
{

public:
	Resource(ToolWindow *, const std::string &rc_file);
	~Resource(void);

	struct FRAME frame;
	struct LABEL label;
	struct POSITION position;
	struct ENVELOPE envelope;
	struct SHOW show;
	struct REPORT report;
	SpoolList *spoolfile;

/*	unsigned int getBevelWidth(void)
	{
		return frame.bevelWidth;
	}
	bt::Color getBorderColor(void)
	{
		return frame.texture.color();
	}
	unsigned int getBorderWidth(void)
	{
		return 0;
	}
*/
	bool validMailbox(void) { return valid_mailbox; }

protected:
	void  Load(void);

private:
	void Frame(void);
	void SizeAndPosition(void);
	void MailCheckSettings(void);
	void Label(void);
	void Show(void);
	void Envelope(void);
	void MailboxDep(void);
	void Clean(void);

	bool valid_mailbox;
	ToolWindow *bbtool;
};
#endif /* __RESOURCE_H */

